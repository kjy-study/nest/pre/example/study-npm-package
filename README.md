# NPM Package
* 폴더 생성
  ```sh
  mkdir 프로젝트명
  cd 프로젝트명
  ```
* npm 패키지 생성

  ```sh
  npm init
  ```

* 타입스크립트 설치
  ```sh
  npm i -D typescript
  ```

* tsconfig.json 파일 생성
  ```sh
  npx tsc --init
  ```

* tsconfig.json 파일 수정
  * declaration 주석 해제
  * declarationMap 주석 해제
  * sourceMap 주석 해제
  * outdir 수정 `"outDir": "lib",`
  * `"include": [
    "src"
  ]` 추가 

  tsconfig.json 파일
  ```json
    {
  "compilerOptions": {
    /* Visit https://aka.ms/tsconfig to read more about this file */

    /* Projects */
    // "incremental": true,                              /* Save .tsbuildinfo files to allow for incremental compilation of projects. */
    // "composite": true,                                /* Enable constraints that allow a TypeScript project to be used with project references. */
    // "tsBuildInfoFile": "./.tsbuildinfo",              /* Specify the path to .tsbuildinfo incremental compilation file. */
    // "disableSourceOfProjectReferenceRedirect": true,  /* Disable preferring source files instead of declaration files when referencing composite projects. */
    // "disableSolutionSearching": true,                 /* Opt a project out of multi-project reference checking when editing. */
    // "disableReferencedProjectLoad": true,             /* Reduce the number of projects loaded automatically by TypeScript. */

    /* Language and Environment */
    "target": "es2016",                                  /* Set the JavaScript language version for emitted JavaScript and include compatible library declarations. */
    // "lib": [],                                        /* Specify a set of bundled library declaration files that describe the target runtime environment. */
    // "jsx": "preserve",                                /* Specify what JSX code is generated. */
    // "experimentalDecorators": true,                   /* Enable experimental support for TC39 stage 2 draft decorators. */
    // "emitDecoratorMetadata": true,                    /* Emit design-type metadata for decorated declarations in source files. */
    // "jsxFactory": "",                                 /* Specify the JSX factory function used when targeting React JSX emit, e.g. 'React.createElement' or 'h'. */
    // "jsxFragmentFactory": "",                         /* Specify the JSX Fragment reference used for fragments when targeting React JSX emit e.g. 'React.Fragment' or 'Fragment'. */
    // "jsxImportSource": "",                            /* Specify module specifier used to import the JSX factory functions when using 'jsx: react-jsx*'. */
    // "reactNamespace": "",                             /* Specify the object invoked for 'createElement'. This only applies when targeting 'react' JSX emit. */
    // "noLib": true,                                    /* Disable including any library files, including the default lib.d.ts. */
    // "useDefineForClassFields": true,                  /* Emit ECMAScript-standard-compliant class fields. */
    // "moduleDetection": "auto",                        /* Control what method is used to detect module-format JS files. */

    /* Modules */
    "module": "commonjs",                                /* Specify what module code is generated. */
    // "rootDir": "./",                                  /* Specify the root folder within your source files. */
    // "moduleResolution": "node",                       /* Specify how TypeScript looks up a file from a given module specifier. */
    // "baseUrl": "./",                                  /* Specify the base directory to resolve non-relative module names. */
    // "paths": {},                                      /* Specify a set of entries that re-map imports to additional lookup locations. */
    // "rootDirs": [],                                   /* Allow multiple folders to be treated as one when resolving modules. */
    // "typeRoots": [],                                  /* Specify multiple folders that act like './node_modules/@types'. */
    // "types": [],                                      /* Specify type package names to be included without being referenced in a source file. */
    // "allowUmdGlobalAccess": true,                     /* Allow accessing UMD globals from modules. */
    // "moduleSuffixes": [],                             /* List of file name suffixes to search when resolving a module. */
    // "resolveJsonModule": true,                        /* Enable importing .json files. */
    // "noResolve": true,                                /* Disallow 'import's, 'require's or '<reference>'s from expanding the number of files TypeScript should add to a project. */

    /* JavaScript Support */
    // "allowJs": true,                                  /* Allow JavaScript files to be a part of your program. Use the 'checkJS' option to get errors from these files. */
    // "checkJs": true,                                  /* Enable error reporting in type-checked JavaScript files. */
    // "maxNodeModuleJsDepth": 1,                        /* Specify the maximum folder depth used for checking JavaScript files from 'node_modules'. Only applicable with 'allowJs'. */

    /* Emit */
    "declaration": true,                              /* Generate .d.ts files from TypeScript and JavaScript files in your project. */
    "declarationMap": true,                           /* Create sourcemaps for d.ts files. */
    // "emitDeclarationOnly": true,                      /* Only output d.ts files and not JavaScript files. */
    "sourceMap": true,                                /* Create source map files for emitted JavaScript files. */
    // "outFile": "./",                                  /* Specify a file that bundles all outputs into one JavaScript file. If 'declaration' is true, also designates a file that bundles all .d.ts output. */
    "outDir": "lib",                                   /* Specify an output folder for all emitted files. */
    // "removeComments": true,                           /* Disable emitting comments. */
    // "noEmit": true,                                   /* Disable emitting files from a compilation. */
    // "importHelpers": true,                            /* Allow importing helper functions from tslib once per project, instead of including them per-file. */
    // "importsNotUsedAsValues": "remove",               /* Specify emit/checking behavior for imports that are only used for types. */
    // "downlevelIteration": true,                       /* Emit more compliant, but verbose and less performant JavaScript for iteration. */
    // "sourceRoot": "",                                 /* Specify the root path for debuggers to find the reference source code. */
    // "mapRoot": "",                                    /* Specify the location where debugger should locate map files instead of generated locations. */
    // "inlineSourceMap": true,                          /* Include sourcemap files inside the emitted JavaScript. */
    // "inlineSources": true,                            /* Include source code in the sourcemaps inside the emitted JavaScript. */
    // "emitBOM": true,                                  /* Emit a UTF-8 Byte Order Mark (BOM) in the beginning of output files. */
    // "newLine": "crlf",                                /* Set the newline character for emitting files. */
    // "stripInternal": true,                            /* Disable emitting declarations that have '@internal' in their JSDoc comments. */
    // "noEmitHelpers": true,                            /* Disable generating custom helper functions like '__extends' in compiled output. */
    // "noEmitOnError": true,                            /* Disable emitting files if any type checking errors are reported. */
    // "preserveConstEnums": true,                       /* Disable erasing 'const enum' declarations in generated code. */
    // "declarationDir": "./",                           /* Specify the output directory for generated declaration files. */
    // "preserveValueImports": true,                     /* Preserve unused imported values in the JavaScript output that would otherwise be removed. */

    /* Interop Constraints */
    // "isolatedModules": true,                          /* Ensure that each file can be safely transpiled without relying on other imports. */
    // "allowSyntheticDefaultImports": true,             /* Allow 'import x from y' when a module doesn't have a default export. */
    "esModuleInterop": true,                             /* Emit additional JavaScript to ease support for importing CommonJS modules. This enables 'allowSyntheticDefaultImports' for type compatibility. */
    // "preserveSymlinks": true,                         /* Disable resolving symlinks to their realpath. This correlates to the same flag in node. */
    "forceConsistentCasingInFileNames": true,            /* Ensure that casing is correct in imports. */

    /* Type Checking */
    "strict": true,                                      /* Enable all strict type-checking options. */
    // "noImplicitAny": true,                            /* Enable error reporting for expressions and declarations with an implied 'any' type. */
    // "strictNullChecks": true,                         /* When type checking, take into account 'null' and 'undefined'. */
    // "strictFunctionTypes": true,                      /* When assigning functions, check to ensure parameters and the return values are subtype-compatible. */
    // "strictBindCallApply": true,                      /* Check that the arguments for 'bind', 'call', and 'apply' methods match the original function. */
    // "strictPropertyInitialization": true,             /* Check for class properties that are declared but not set in the constructor. */
    // "noImplicitThis": true,                           /* Enable error reporting when 'this' is given the type 'any'. */
    // "useUnknownInCatchVariables": true,               /* Default catch clause variables as 'unknown' instead of 'any'. */
    // "alwaysStrict": true,                             /* Ensure 'use strict' is always emitted. */
    // "noUnusedLocals": true,                           /* Enable error reporting when local variables aren't read. */
    // "noUnusedParameters": true,                       /* Raise an error when a function parameter isn't read. */
    // "exactOptionalPropertyTypes": true,               /* Interpret optional property types as written, rather than adding 'undefined'. */
    // "noImplicitReturns": true,                        /* Enable error reporting for codepaths that do not explicitly return in a function. */
    // "noFallthroughCasesInSwitch": true,               /* Enable error reporting for fallthrough cases in switch statements. */
    // "noUncheckedIndexedAccess": true,                 /* Add 'undefined' to a type when accessed using an index. */
    // "noImplicitOverride": true,                       /* Ensure overriding members in derived classes are marked with an override modifier. */
    // "noPropertyAccessFromIndexSignature": true,       /* Enforces using indexed accessors for keys declared using an indexed type. */
    // "allowUnusedLabels": true,                        /* Disable error reporting for unused labels. */
    // "allowUnreachableCode": true,                     /* Disable error reporting for unreachable code. */

    /* Completeness */
    // "skipDefaultLibCheck": true,                      /* Skip type checking .d.ts files that are included with TypeScript. */
    "skipLibCheck": true                                 /* Skip type checking all .d.ts files. */
  },
  "include": [
    "src"
  ]
  }
  ```

* package.json 파일 수정
  * main - `"main": "lib/index.js",`로 변경
  * types - `"types": "lib"`, 추가
  * scripts - `"build": "tsc -p .",` 추가
  * name - 필수는 아니나, `@scope`를 추가한다.

  package.json
  ```json
  {
    "name": "@mion-2/study-npm-package",
    "version": "0.0.1",
    "description": "NPM 패키지 만들기",
    "main": "lib/index.js",
    "types": "lib",
    "scripts": {
      "build": "tsc -p ."
    },
    "author": "mion",
    "license": "ISC",
    "devDependencies": {
      "typescript": "^4.7.4"
    }
  }

  ```

* IDE에서 src 폴더 및 index.ts 추가
* index.ts에 테스트 함수 추가
  ```ts
  export const printStudyLibrary = () => {console.log("Study Library");} 
  ```

* 패키지 빌드
  ```
  npm run build
  ```
  작업이 완료되면 lib 폴더가 생성됩니다.


* NPM 게시
  * NPM 로그인이 되어있지 않다면 로그인을 합니다.
    `npm login `
  * NPM 게시하기
    * private 게시 : `npm publish`
    * public 게시 : `npm publish --access public`

# Gitlab Package Repository
npm의 private 저장소를 사용하려면 비용을 지불해야 합니다. Gitlab의 Package Repository에 라이브러리를 업로드해서 무료로 사용할 수 있습니다.  
[공식문서](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry)   
[동영상](https://www.youtube.com/watch?v=OqS6jb22AFE&ab_channel=AllThingsCS)

* gitlab 홈페이지에서 사용자의 이름을 확인합니다.(내 계정에서 '@'로 시작합니다. 다르게 설정해도 됩니다.)
* package.json의 name에 '@사용자지정이름/패키지명' 으로 이름을 변경합니다. 
**사용자 이름 및 패키지명은 모두 소문자로 입력 합니다.**
 * 게시할 Repository 지정
    * package.json에 다음 구문을 추가하여 게시할 저장소를 정합니다.
      ```
      "publishConfig": { "@mion:registry":" https://gitlab.com/api/v4/projects/37634818/packages/npm/" }
      ```
  ```json
  {
  "name": "@mion/study-npm-package",
  "version": "0.0.3",
  "description": "NPM 패키지 만들기",
  "main": "./lib/index.js",
  "types": "./lib/index.ts",
  "scripts": {
    "build": "tsc -p ."
  },
  "author": "mion",
  "license": "ISC",
  "devDependencies": {
    "typescript": "^4.7.4"
  },
  "publishConfig": { "@mion:registry":" https://gitlab.com/api/v4/projects/37634818/packages/npm/" }
  }
  ```

* npm 셋팅
  * gitlab > 해당 프로젝트 > 설정 > 저장소 > Deploy Token
    * read_package_registry, write_package_registry 체크하고 토큰 발급.

  * 프로젝트에서 .npmrc 파일을 만들고 아래 내용을 붙여넣습니다.   
    여기서 _authToken에 토큰을 입력합니다.
    ```
    # 프로젝트 수준 설정 시
    #@foo:registry=https://gitlab.example.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/
    #//gitlab.example.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}

    # 인스턴스 수준 설정 시
    #@foo:registry=https://gitlab.example.com/api/v4/packages/npm/
    #//gitlab.example.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}

    @mion:registry=https://gitlab.com/api/v4/projects/37634818/packages/npm/
    //gitlab.com/api/v4/projects/37634818/packages/npm/:_authToken=2gip7BQsWhtd9vmRMt9S

    ```

  * 프로젝트에서 .npmignore 파일을 생성하고 패키지에서 제외할 디렉토리 이름을 기재합니다.
    ```
    /src
    ```

* CI/CD 셋팅
  * .gitlab-ci.yml 파일을 생성하고 아래 코드를 복붙 합니다.
  ```
    image: node:latest

    stages:
      - deploy

    deploy:
      stage: deploy
      script:
        - echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}">.npmrc
        - npm publish
  ```
  * gitlab > 설정 > CI/CD > 변수에 CI_JOB_TOKEN의 값을 추가합니다.
  
  ## 다른 프로젝트에서 사용하기
    * Resitry setup 경로를 복사 합니다.

      ![](./images/2022-07-07-14-32-31.png)
    * .npmrc 파일을 생성하고, 터미널로 해당 내용을 붙여넣습니다.
    * .npmrc에 토큰 값도 넣습니다.

      ![](./images/2022-07-07-14-34-51.png)
    * `npm i @mion/study-npm-package` 으로 패키지를 다운 받습니다.
